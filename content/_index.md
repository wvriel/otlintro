---
title: OTL for public infrastructure
---
<!--: .wrap .size-70 ..aligncenter bgimage=images/background.png -->


## **OTLs for public infrastructure**

<!--: .text-intro -->A short introduction to a TenneT case

[Wouter van Riel](https://www.linkedin.com/in/wouter-van-riel/)

---
<!--: .wrap .fadeInUp bg=bg-black bg=aligncenter bgimage=frame|images/sewers.jpg -->

## **Who is Wouter?**

---

<!-- : .wrap -->

|||v

### **Some OTL basics**

<!-- : .text-intro -->*Imagine a dictionary*

![Text](images/dictionary.jpg)

<!-- : .text-intro -->OTL is a uniform framework for the definition of assets, their attributes and their relations.

... could also contain activities, documents, asset state, regions,

|||

![Text](images/otlcopy.png)

---

<!--: bg=bg-apple -->

### **Why?**

<!-- : .text-intro -->Cooperation requires proper communication

<div>
 <iframe style="border: 0; width:100%; height:600px; overflow: auto" src="images/problem2.png">
 </iframe>
</div>

---
<!-- : .wrap -->

|||v

### **Allright, let's put stuff to practice**

<!-- : .text-intro -->The coding way

A (very tiny) example

|||

~~~ttl

:BatteryUnit a owl:Class ;
    rdfs:comment "An electrochemical energy storage device"@en ;
    rdfs:subClassOf :PowerElectronicsUnit ;
    skos:prefLabel "BatteryUnit"@en .

~~~

---
<!-- : .wrap -->

|||v

### **Allright, let's put stuff to practice**

<!-- : .text-intro -->The visualised way

A more mature example of the urban drainage sector

|||

[Data Dictionary for Urban Drainage](https://data.gwsw.nl/)

[Publicly available data](https://www.pdok.nl/viewer/)

---

<!-- : bg=bg-apple .aligncenter -->

## Any questions?

<a href="mailto:wouter.van.riel@infralytics.org">wouter.van.riel@infralytics.org</a>
